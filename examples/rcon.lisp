;; -*- Mode: lisp; indent-tabs-mode: nil -*-
;; 
;; Copyright (c) 2018 Momchil Ivanov <momchil@xaxo.eu>
;;

(require :unix-opts)
(require :cl-readline)
(require :cl-rcon)

(defvar *host* "localhost")
(defvar *port* 27015)
(defvar *password* "test")
(defvar *history-file* (concatenate 'string
                                    (namestring (user-homedir-pathname))
                                    ".history.rcon"))

(opts:define-opts
  (:name :help
         :description "print this help text"
         :short #\h
         :long "help")
  (:name :host
         :description "hostname"
         :short #\H
         :long "host"
         :arg-parser #'identity
         :meta-var "HOST")
  (:name :port
         :description "port"
         :short #\p
         :long "port"
         :arg-parser #'parse-integer
         :meta-var "PORT")
  (:name :password
         :description "password"
         :short #\P
         :long "password"
         :arg-parser #'identity
         :meta-var "PASSWORD")
  (:name :exec
         :description "command to execute"
         :short #\e
         :long "exec"
         :arg-parser #'identity
         :meta-var "COMMAND"))

(defmacro when-option ((options opt) &body body)
  `(let ((it (getf ,options ,opt)))
     (when it
       ,@body)))

(defun usage ()
  (opts:describe :usage-of "rcon")
  (opts:exit))        

(defun novelty-check (x y)
  (string/= (string-trim " " x)
            (string-trim " " y)))

(defun repl (rcon)
  (rl:read-history *history-file*)
  (loop
     for line =  (rl:readline :prompt (format nil "-> ")
                              :add-history t
                              :novelty-check #'novelty-check)
     while line
     if (> (length line) 4086)
     do (format t "ERROR: Command too long.~&")
       (finish-output)
     else
     do
       (format t "~A~&" (rcon:send rcon line)))
  (rcon:shutdown rcon)
  (rl:write-history *history-file*))

(defun my-quit (&optional (status 0))
  "Exit the program returning `status'."
  #+sbcl      (sb-ext:exit :code status)
  #+cmu       (unix:unix-exit status)
  #+ccl       (ccl:quit status)
  #+ecl       (ext:quit status)
  #+clisp     (ext:exit status)
  #+abcl      (extensions:exit :status status)
  #+allegro   (excl:exit status :quiet t)
  #+lispworks (lispworks:quit :status status))

(defun main ()
  (multiple-value-bind (options free-args)
      (handler-case
          (opts:get-opts)
        (opts:arg-parser-failed (condition)
          (format t "ERROR: cannot parse ~s as argument of ~s~%"
                  (opts:raw-arg condition)
                  (opts:option condition))
          (my-quit 1))
        (opts:unknown-option () (usage)))
    
    (declare (ignore free-args))
    
    (when-option (options :help)
                 (usage))

    (when-option (options :host)
                 (setf *host* it))
    
    (when-option (options :port)
                 (setf *port* it))

    (when-option (options :password)
                 (setf *password* it))

    (handler-case
        (let ((rcon (rcon:connect *host* *port* *password*)))
          
          (if (getf options :exec)
              (progn
                (format t "~A~&" (rcon:send rcon (getf options :exec))))
              (repl rcon))
          (rcon:shutdown rcon))
      (rcon:authentication-error (c)
        (format t "ERROR: Authentication error!~&")
        (format t "~A~&" (rcon::text c))
        (my-quit 1))
      (usocket:connection-refused-error ()
        (format t "ERORR: Cannot connect to server!~&")
        (my-quit 1)))))

(main)
