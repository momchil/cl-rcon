;; -*- Mode: lisp; indent-tabs-mode: nil -*-
;; 
;; Copyright (c) 2018 Momchil Ivanov <momchil@xaxo.eu>
;;

(in-package :cl-rcon)

(define-constant +packet-types+
    '((:auth           . 3)
      (:auth-response  . 2)
      (:execcommand    . 2)
      (:response-value . 0))
  :test #'equal
  :documentation "List of packet types.")

(defun packet-type-to-int (type)
  "Returns integer value of type."
  (cdr (assoc type +packet-types+)))

(defun int-to-packet-type (i)
  "Returns integer value of type."
  (car (rassoc i +packet-types+)))

(defstruct packet
  "Packet structure."
  (id 0 :type (unsigned-byte 32))
  (type 0 :type (unsigned-byte 32))
  (body))

(defun packet (id type body)
  "Creates a packet."
  (make-packet :id id
               :type (packet-type-to-int type)
               :body body))

(defun is-packet-type (packet type)
  "Returns t if packet is of type type."
  (= (packet-type packet) (packet-type-to-int type)))

(defun write-byte-32-to-byte-array (array i &optional (offset 0))
  "Write byte-32 integer in an array of bytes. Use Little-endian."
  (setf  (aref array offset) (ldb (byte 8  0) i)
  (aref array (+ offset 1))  (ldb (byte 8  8) i)
  (aref array (+ offset 2))  (ldb (byte 8 16) i)
  (aref array (+ offset 3))  (ldb (byte 8 24) i)))

(defun read-byte-array-to-byte-32 (byte-array &optional (offset 0))
  "Read a 32-bit integer from an array of bytes. Use Little-endian."
  (let ((i 0))
    (setf (ldb (byte 8  0) i) (aref byte-array offset)
          (ldb (byte 8  8) i) (aref byte-array (+ offset 1))
          (ldb (byte 8 16) i) (aref byte-array (+ offset 2))
          (ldb (byte 8 24) i) (aref byte-array (+ offset 3)))
    i))

(defun well-formed-packet-p (vector &optional (start 0) end)
  "Checks if vector contains a well-formed packet."
  ;; full packet size must be at least 10
  (let ((len (- (or end (length vector))
                start)))
    (when (>= len 14)
      ;; the size field is 4 bytes long
      (let ((size (read-byte-array-to-byte-32 vector start)))
        (and
         ;; packet size is size + 4
         (= len (+ 4 size))
         ;; valid packet type
         (int-to-packet-type
          (read-byte-array-to-byte-32 vector (+ 8 start)))
         ;; match the two 0x0 at the end of the packet
         (and (= 0 (elt vector (+ 2 size start)))
              (= 0 (elt vector (+ 3 size start)))))))))

(defun parse-packet (vector &optional (start 0) end)
  "If vector contains a valid packet return it."
  (if (well-formed-packet-p vector start end)
      (binary-to-packet vector start end)
      (error 'malformed-packet-error :data vector)))

;; wrap in eval-when to allow the definition of the
;; +control-packet-string+ constant, otherwise compiler complains with
;; undefined function
(eval-when (:compile-toplevel :load-toplevel :execute)
  (defun binary-to-string (vector &optional (start 0) end)
    "Converts byte array to string."
    (babel:octets-to-string vector :start start :end end :encoding :utf-8)))

(defun string-to-binary (str)
  (babel:string-to-octets str :encoding :utf-8))

(defun packet-to-binary (packet)
  "Creates an rcon packet.

The structure of the packet is:

  Byte  | Field         | Description
  -----------------------------------------------------------
  0-3   | Size          | 32-bit little-endian Signed Integer
  4-7   | ID            | 32-bit little-endian Signed Integer
  8-11  | Type          | 32-bit little-endian Signed Integer
  12-X  | Body          | Null-terminated ASCII String (0x00)
  XXXX  | Empty String  | Null-terminated ASCII String (0x00)


Packet Size

The packet size field is a 32-bit little endian integer, representing
the length of the request in bytes. Note that the packet size field
itself is not included when determining the size of the packet, so the
value of this field is always 4 less than the packet's actual
length. The minimum possible value for packet size is 10:

  4 bytes (ID field)
  4 bytes (Type field)
  at least 1 byte (packet body)
  1 byte (empty string terminator).

Packet ID

The packet id field is a 32-bit little endian integer chosen by the client for each request. It may be set to any positive integer. When the server responds to the request, the response packet will have the same packet id as the original request (unless it is a failed SERVERDATA_AUTH_RESPONSE packet - see below.) It need not be unique, but if a unique packet id is assigned, it can be used to match incoming responses to their corresponding requests.

Packet Type

The packet type field is a 32-bit little endian integer, which indicates the purpose of the packet. Its value will always be either 0, 2, or 3, depending on which of the following request/response types the packet represents:

Value   String Descriptor
---------------------------------
3       SERVERDATA_AUTH
2       SERVERDATA_AUTH_RESPONSE
2       SERVERDATA_EXECCOMMAND
0       SERVERDATA_RESPONSE_VALUE

Note that the repetition in the above table is not an error: SERVERDATA_AUTH_RESPONSE and SERVERDATA_EXECCOMMAND both have a numeric value of 2.

Packet Body

The packet body field is a null-terminated string encoded in ASCII (i.e. ASCIIZ). Depending on the packet type, it may contain either the RCON password for the server, the command to be executed, or the server's response to a request.

Empty String

The end of a Source RCON Protocol packet is marked by an empty ASCIIZ string - essentially just 8 bits of 0. In languages that do not null-terminate strings automatically, you can simply write 16 bits of 0 immediately after the packet body (8 to terminate the packet body and 8 for the empty string afterwards).

Source: https://developer.valvesoftware.com/wiki/Source_RCON_Protocol
"
  (let* ((data (string-to-binary (packet-body packet)))
         (header (make-array 12 :element-type '(unsigned-byte 8))))
    (write-byte-32-to-byte-array header (+ 10 (length data)))
    (write-byte-32-to-byte-array header (packet-id packet) 4)
    (write-byte-32-to-byte-array header (packet-type packet) 8)
    (concatenate '(vector (unsigned-byte 8)) header data #(0) #(0))))

(defun binary-to-packet (vector &optional (start 0) end)
  "Creates a packet structure from a binary vector."
  (let ((size (- (or end (length vector)) start 4)))
    (make-packet ;; the id field is 4 bytes long
                 :id (read-byte-array-to-byte-32 vector (+ 4 start))
                 ;; the type field is 4 bytes long
                 :type (read-byte-array-to-byte-32 vector (+ 8 start))
                 ;; body is the rest of the packet excet the last byte
                 :body (binary-to-string vector (+ 12 start) (+ 2 size start)))))

(define-constant +control-packet-string+
    (binary-to-string (make-array 4
                                  :element-type '(unsigned-byte 8)
                                  :initial-contents
                                  '(0 1 0 0)))
  :test #'equal)

(defun control-packet-p (packet)
  (and (is-packet-type packet :response-value)
       (equalp (packet-body packet) +control-packet-string+)))
