;; -*- Mode: lisp; indent-tabs-mode: nil -*-
;; 
;; Copyright (c) 2018 Momchil Ivanov <momchil@xaxo.eu>
;;

(defpackage :cl-rcon
  (:nicknames :rcon)
  (:use :common-lisp :alexandria)
  (:import-from :alexandria :define-constant)
  (:export
   ;; methods and functions
   #:connect
   #:shutdown
   #:send
   ;; conditions
   #:read-error
   #:packet-error
   #:malformed-packet-error
   #:packet-order-error
   #:authentication-error
   #:command-too-long-error
   ))
