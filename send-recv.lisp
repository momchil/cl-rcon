;; -*- Mode: lisp; indent-tabs-mode: nil -*-
;; 
;; Copyright (c) 2018 Momchil Ivanov <momchil@xaxo.eu>
;;

(in-package :cl-rcon)

(defvar *debug* nil)

(defun socket-connect (host port)
  "Connect to a rcon server on host:port."
  (usocket:socket-connect host port
                          :protocol :stream
                          :element-type '(unsigned-byte 8)))

(defun socket-close (socket)
  "Closes the socket."
  (usocket:socket-close socket))

(defun send-packet (socket packet &optional (finish-output t))
  "Sends packet to socket. Calls finish-output if finish-output is t
to flush the output stream."
  (let* ((vector (packet-to-binary packet))
         (stream (usocket:socket-stream socket))
         (res (write-sequence vector stream)))
    (when finish-output (finish-output stream))
    (when *debug*
      (format t "send: ~A~%" packet)
      (format t "send: ~A~%" vector))
    res))

(defun send-cmd (socket str &optional (id-cmd 0) (id-xxx 1))
  "Sends str in one packet. Sends an empty packet afterwards to
implement multiple-packet responses."
  (send-packet socket (packet id-cmd :execcommand str))
  (send-packet socket (packet id-xxx :response-value "")))

(defun send-auth (socket password &optional (id 0))
  "Sends an authentication packet."
  (send-packet socket (packet id :auth password)))

(defun read-bytes (stream n)
  "Receives a data from stream."
  (let ((vector (make-array n :element-type '(unsigned-byte 8))))
    ;; detect EOF when read-sequence does not return the number of
    ;; bytes we wanted to read
    (if (eq n (read-sequence vector stream))
        (progn
          (when *debug* (format t "rcvd: ~A~%" vector))
          vector)
        (error 'read-error :data (subseq vector 0 n)))))

(defun receive-packet (socket)
  "Reads and returns a packet from socket."
  (let* ((stream (usocket:socket-stream socket))
         (vector (read-bytes stream 4)))
    (when vector
      (let* ((size (read-byte-array-to-byte-32 vector))
             (vector2 (read-bytes stream size)))
        (setf vector (concatenate '(vector (unsigned-byte 8)) vector vector2))
        (let ((packet (parse-packet vector)))
          (when *debug* (format t "rcvd: ~A~%" packet))
          packet)))))

(defun list-to-string (lst)
  (format nil "~{~a~}" lst))

(defun receive-cmd (socket &optional (id-cmd 0) (id-xxx 1))
  "Receives a response to a send command, implements multiple-packet
response."
  (let ((packets (loop for packet = (receive-packet socket)
                    while packet
                    while (= (packet-id packet) id-cmd)
                    while (is-packet-type packet :response-value)
                    collect packet)))
    ;; receive control packet
    (let ((packet (receive-packet socket)))
      (unless (control-packet-p packet)
        (error 'packet-order-error :packet packet)))
  (list-to-string (mapcar #'packet-body packets))))

(defun auth (socket password &optional (id 0))
  "Sends authentication packet and receives authentication response
from the server. Returns nil on error, t otherwise."
  (send-auth socket password id)
  (handler-case
      (let* ((a (receive-packet socket))
             (b (receive-packet socket)))
        (or
         (and a b
              (is-packet-type a :response-value)
              (is-packet-type b :auth-response)
              (= (packet-id a) id)
              (= (packet-id b) id))
         (error 'authentication-error :text (packet-body a))))
    (read-error ()
      ;; in case the server banned us it is not responding at all,
      ;; therefore catch read-error and emit authentication-error
      (error 'authentication-error
             :text "Server closed the connection unexpectedly. Probably ban."))))
