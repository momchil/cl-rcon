;; -*- Mode: lisp; indent-tabs-mode: nil -*-
;; 
;; Copyright (c) 2018 Momchil Ivanov <momchil@xaxo.eu>
;;

(in-package :cl-rcon)

(defclass rcon ()
  ((socket
    :initarg :socket
    :accessor socket
    :documentation "usocket socket.")
   (last-packet-id
    :initarg :last-packet-id
    :initform 0
    :type (unsigned-byte 32)
    :accessor last-packet-id
    :documentation "id of the last sent packet.")
   (last-cmd-packet-id
    :initarg :last-cmd-packet-id
    :initform 0
    :type (unsigned-byte 32)
    :accessor last-cmd-packet-id
    :documentation "id of the last sent command packet.")
   (fn-id
    :initarg :fn-id
    :initform #'random-id
    :accessor fn-id
    :documentation "function generating the next packet id given one."
   )))

(defun connect (host port password &optional (fn-id #'random-id))
  "Creates an RCON connection to HOST:POST and authenticates with
PASSWORD. Returns an RCON object on success."
  (check-type password string)
  (check-type port integer)
  (let* ((socket (socket-connect host port))
         (rcon (make-instance 'rcon :socket socket :fn-id fn-id)))
    (auth socket password (next-packet-id rcon))
    rcon))

(defun random-id (x)
  (declare ((unsigned-byte 32) x) (ignore x))
  ;; 2^32 - 1
  (random 4294967295))

(defmethod shutdown ((rcon rcon))
  "Closes an RCON connection."
  (socket-close (socket rcon)))

(defmethod receive ((rcon rcon))
  (receive-cmd (socket rcon) (last-cmd-packet-id rcon) (last-packet-id rcon)))

(defmethod next-packet-id ((rcon rcon))
  (setf (last-packet-id rcon) (funcall (fn-id rcon) (last-packet-id rcon))))

(defmethod next-cmd-packet-id ((rcon rcon))
  (setf (last-cmd-packet-id rcon) (next-packet-id rcon)))

(defmethod new-send-ids ((rcon rcon))
  (let ((x (next-cmd-packet-id rcon)))
    (loop for y = (next-packet-id rcon)
       until (/= x y)
       finally (return (values x y)))))

(defmethod send ((rcon rcon) str)
  "Sends an RCON command STR, receives and returns the response."
  (check-type str string)
  (when (> (length str) 4086)
    (error 'command-too-long-error :text str))
  (multiple-value-bind (x y)
      (new-send-ids rcon)
    (send-cmd (socket rcon) str x y))
  (receive rcon))
