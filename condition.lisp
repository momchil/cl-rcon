;; -*- Mode: lisp; indent-tabs-mode: nil -*-
;; 
;; Copyright (c) 2018 Momchil Ivanov <momchil@xaxo.eu>
;;

(in-package :cl-rcon)

(define-condition read-error (error)
  ((data :initarg :data :reader data)))

(define-condition packet-error (error)
  ((data :initarg :data :reader data)))

(define-condition malformed-packet-error (packet-error)
  ((data :initarg :data :reader data)))

(define-condition packet-order-error (packet-error)
  ((packet :initarg :packet :reader packet)))

(define-condition authentication-error (error)
  ((text :initarg :text :reader text)))

(define-condition command-too-long-error (error)
  ((text :initarg :text :reader text)))
