1. Introduction

  This is a Common Lisp implementation of the RCON protocol described
  in Reference [1].

2. Required systems

  The systems used by cl-rcon are usocket and babel.

3. Application Interface (API)

  The system exports the following functions and methods:

    - connect (host port password &optional fn-id)
    
      Returns a rcon object when a successfull connection to the rcon
      server on host:port with the password was establised. The port
      argument must be an integer, the password argument bust be a
      string. The optional argument fn-id specifies a function of the
      form f (x) that is used to generate the next packet id given the
      last one x. The packet id is 32 bits long.

    - shutdown ((rcon rcon))

      Closes the rcon connection.
      
    - send ((rcon rcon) str)

      Sends the string str to the server and returns the response as a
      string.

4. Errors

  The system exports the following errors:
  
    read-error
    packet-error
    malformed-packet-error
    packet-order-error
    authentication-error
    command-too-long-error

5. Examples

  Connect to a server and send "status":
  
    (handler-case
      (let ((rcon (rcon:connect host port password)))
        (format t "~A~&"
                (rcon:send rcon "status"))
        (rcon:shutdown rcon))
    (rcon:authentication-error (c)
      (format t "ERROR: Authentication error!~&"))
    (usocket:connection-refused-error ()
      (format t "ERORR: Connection refused!~&")))


6. RCON client

  An implementation of a RCON client program is located in the bin/
  folder.

7. Copyright

  Copyright (c) 2018 Momchil Ivanov <momchil@xaxo.eu> 
  All rights reserved.

8. References

  1: http://developer.valvesoftware.com/wiki/Source_RCON_Protocol
