;;;; -*- Mode: lisp; indent-tabs-mode: nil -*-
;;;; 
;;;; Copyright (c) 2018 Momchil Ivanov <momchil@xaxo.eu>
;;;;

(in-package :asdf)

(defsystem cl-rcon
  :name "cl-rcon"
  :author "Momchil Ivanov"
  :maintainer "Momchil Ivanov"
  :version "1.0"
  :license "Copyright (c) 2018"
  :description "Common Lisp implementation of the RCON protocol"
  :depends-on (alexandria usocket babel)
  :serial t
  :components((:file "package")
              (:file "condition")
              (:file "packet")
              (:file "send-recv")
              (:file "rcon")))
